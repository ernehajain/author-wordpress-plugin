<?php
/**
 * @package Author
 */
/*
Plugin Name: Toptal Author
Plugin URI: http://toptal.com/
Description: This plug-in provides an Custom Post Type (Author) over admin end. By using this, Admin can add authors and get it linked with existing WordPress user along with that it displays the author's details over front end. 
Version: 1.0
Author: Neha (Created for Toptal)
Author URI: https://www.linkedin.com/in/neha-jain-software-engineer/
License: Later
Text Domain: Author
*/
/*
This plug-in provides an Custom Post Type (Author) over admin end. By using this, Admin can add authors and get it linked with existing WordPress user along with that it displays the author's details over front end.

Copyright 2016-2017 Neha Rani, Inc.
*/
	//Including Required files from library folder
	
	require_once( plugin_dir_path( __FILE__ ) . 'library/enqueuing.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'library/metabox.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'library/ajax-update.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'library/readmethods.php' );
	//Binding the custom functions with  hooks
	add_action( 'add_meta_boxes', 'toptal_authors_register_metabox' );
	add_action( 'save_post', 'toptal_authors_save_perm_metadata', 1, 2 );
	add_action( 'admin_enqueue_scripts', 'toptal_authors_admin_enqueue_files' );
	add_action( 'wp_enqueue_scripts', 'toptal_authors_front_enqueue_files', 100 );
	add_action( 'wp_ajax_toptal_authors_update_temp', 'toptal_authors_update_temp' );
	add_action( 'add_meta_boxes_authors', 'authors_post_binding_meta' );
	add_action( 'publish_authors', 'insert_meta_box_db' );
	add_action( 'toptal_authors_post_types', 'add_image_galleries_on_author' );
	//Calling the create_authors function 
	add_action( 'init', 'create_authors' );
	/*
	 * Function name : create_authors
	 * Usage : This function is used to Register custom post type Author 
	 * Parameters : None
	 */
	function create_authors()
	{
			//Setting labels for Authors Post type
			$labels = array(
					 'name' => __( 'Authors', 'Post Type General Name' ),
					'singular_name' => __( 'Author', 'Post Type Singular Name' ),
					'menu_name' => __( 'Authors' ),
					'parent_item_colon' => __( 'Parent Author' ),
					'all_items' => __( 'All Authors' ),
					'view_item' => __( 'View Author' ),
					'add_new_item' => __( 'Add New Author' ),
					'add_new' => __( 'Add New' ),
					'edit_item' => __( 'Edit Author' ),
					'update_item' => __( 'Update Author' ),
					'search_items' => __( 'Search Author' ),
					'not_found' => __( 'Not Found' ),
					'not_found_in_trash' => __( 'Not found in Trash' ) 
			);
			// Set other options for Author Post Type
			$args   = array(
					 'label' => __( 'authors' ),
					'description' => __( 'Author for toptal' ),
					'labels' => $labels,
					'supports' => array(
							 'thumbnail',
							
					),
					'hierarchical' => false,
					'public' => true,
					'show_ui' => true,
					'show_in_menu' => true,
					'show_in_nav_menus' => true,
					'show_in_admin_bar' => true,
					'menu_position' => 7,
					'can_export' => true,
					'has_archive' => true,
					'exclude_from_search' => false,
					'publicly_queryable' => true 
			);
			// Registering Authors Post Type
			register_post_type( 'authors', $args );
	}
	add_action( 'admin_head-post-new.php', 'change_thumbnail_html' );
	add_action( 'admin_head-post.php', 'change_thumbnail_html' );
	/*
	 * Function name : change_thumbnail_html
	 * Usage : This function is used to Calls function - replace_set_featured_image_text if post type is authors
	 * Parameters : content
	 */
	function change_thumbnail_html( $content )
	{
			if ( 'authors' == $GLOBALS['post_type'] ) {
					add_filter( 'admin_post_thumbnail_html', 'replace_set_featured_image_text' );
			}
	}
	/*
	 * Function name : replace_set_featured_image_text
	 * Usage : This function is used to change the label used for featured image
	 * Parameters : content
	 */
	function replace_set_featured_image_text( $content )
	{
			$content = str_replace( __( 'Set featured image' ), __( 'Set Author Avatar' ), $content );
			$content = str_replace( __( 'Remove featured image' ), __( 'Remove Author Avatar' ), $content );
			return $content;
	}
	add_filter( 'single_template', 'single_author_template' );
	/*
	 *	Function name : single_author_template
	 *	Usage : This function is used to overwrite the single.php for author post type	
	 *  Parameters : single
	 */
	function single_author_template( $single )
	{
			global $wp_query, $post;
			/* Checks for single template by post type */
			if ( $post->post_type == "authors" ) {
					if ( file_exists( plugin_dir_path( __FILE__ ) . '/single-author.php' ) )
							return plugin_dir_path( __FILE__ ) . '/single-author.php';
			}
			return $single;
	}
	add_filter( 'archive_template', 'archive_author_template' );
	/*
	 *	Function name : archive_author_template
	 *	Usage : This function is used to overwrite the archive.php for author post type	
	 *  Parameters : single
	 */
	function archive_author_template( $archive_template )
	{
			global $post;
			if ( is_post_type_archive( 'authors' ) ) {
					$archive_template = dirname( __FILE__ ) . '/archive-author.php';
			}
			return $archive_template;
	}
