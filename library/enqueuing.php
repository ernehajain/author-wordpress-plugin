<?php
/*
 * Function name : toptal_authors_enqueue_files
 * Usage : This function includes CSS and JS files on admin end when plug-in gets activated
 */
function toptal_authors_admin_enqueue_files()
{
		wp_enqueue_script( 'toptal-authors-script-admin', plugin_dir_url( dirname( __FILE__ ) ) . 'js/admin.js' );
		wp_enqueue_script( 'toptal-authors-script-admin', 'myAjax', array(
				 'ajaxurl' => admin_url( 'admin-ajax.php' ) 
		) );
		wp_enqueue_style( 'toptal-authors-style-admin', plugin_dir_url( dirname( __FILE__ ) ) . 'css/admin.css' );
		wp_enqueue_script( 'custom_developer_validation', plugin_dir_url( dirname( __FILE__ ) ) . 'js/developer.js' );
		wp_enqueue_style( 'custom_developer_style', plugin_dir_url( dirname( __FILE__ ) ) . 'css/developer.css' );
		
}
/*
 * Function name : toptal_authors_front_enqueue_files
 * Usage : This function includes CSS and JS files on admin end when plug-in gets activated
 */
function toptal_authors_front_enqueue_files()
{
		wp_enqueue_script( 'bootstrap_min_js', plugin_dir_url( dirname( __FILE__ ) ) . 'js/bootstrap.min.js' );
		wp_enqueue_style( 'bootstrap_min_css', plugin_dir_url( dirname( __FILE__ ) ) . 'css/bootstrap.min.css' );
		wp_enqueue_style( 'landing_page_css', plugin_dir_url( dirname( __FILE__ ) ) . 'css/landing-page.css' );
		wp_enqueue_style( 'developer_css', plugin_dir_url( dirname( __FILE__ ) ) . 'css/developer.css' );
		wp_enqueue_style( 'font_awesome_css', plugin_dir_url( dirname( __FILE__ ) ) . 'font-awesome/css/font-awesome.min.css' );
		wp_enqueue_style( 'googleFonts', 'https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic' );
}
