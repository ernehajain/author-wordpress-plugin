=== Toptap AUthor ===
Contributors: Neha Rani
Tags: authors, users
Requires at least: 3.2
Tested up to: 4.6
License: later

Toptal authors plugin provides an Custom Post Type (Author) over admin end. By using this, Admin can add authors and get it linked with existing WordPress user along with that it displays the author's details over front end. 

== Description ==

Toptal autors plugin provides an Custom Post Type (Author) over admin end. By using this, Admin can add authors and get it linked with existing WordPress user along with that it displays the author's details over front end. 

Major features in Toptal Authors include:

* Linking with wordpress user and display post in footer on author detail page
* Author listing on archive page.

== Installation ==

Upload the Toptal Author plugin to your plugin directory, Activate it, then add author from authors menu.

Kindly change permalink settings with post name, second last option from permalink radio button
1, 2, 3: You're done!

== Changelog ==

= 1.1 =
*Release Date - 23 August 2016*